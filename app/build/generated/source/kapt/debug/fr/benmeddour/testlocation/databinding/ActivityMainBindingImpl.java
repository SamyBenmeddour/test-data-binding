package fr.benmeddour.testlocation.databinding;
import fr.benmeddour.testlocation.R;
import fr.benmeddour.testlocation.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding implements fr.benmeddour.testlocation.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.LinearLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.TextView mboundView2;
    @NonNull
    private final android.widget.Button mboundView3;
    @NonNull
    private final androidx.recyclerview.widget.RecyclerView mboundView4;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            );
        this.mboundView0 = (android.widget.LinearLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.TextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (android.widget.Button) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.recyclerview.widget.RecyclerView) bindings[4];
        this.mboundView4.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new fr.benmeddour.testlocation.generated.callback.OnClickListener(this, 1);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((fr.benmeddour.testlocation.MainViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable fr.benmeddour.testlocation.MainViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelCount((androidx.lifecycle.MutableLiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeViewModelIsMessageVisible((androidx.lifecycle.MutableLiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelNumbers((androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Integer>>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelCount(androidx.lifecycle.MutableLiveData<java.lang.Integer> ViewModelCount, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelIsMessageVisible(androidx.lifecycle.MutableLiveData<java.lang.Boolean> ViewModelIsMessageVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelNumbers(androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Integer>> ViewModelNumbers, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.MutableLiveData<java.lang.Integer> viewModelCount = null;
        java.util.List<java.lang.Integer> viewModelNumbersGetValue = null;
        java.lang.Boolean viewModelIsMessageVisibleGetValue = null;
        java.lang.Integer viewModelCountGetValue = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelIsMessageVisibleGetValue = false;
        androidx.lifecycle.MutableLiveData<java.lang.Boolean> viewModelIsMessageVisible = null;
        androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Integer>> viewModelNumbers = null;
        fr.benmeddour.testlocation.MainViewModel viewModel = mViewModel;
        java.lang.String integerToStringViewModelCount = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelCountGetValue = 0;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.count
                        viewModelCount = viewModel.getCount();
                    }
                    updateLiveDataRegistration(0, viewModelCount);


                    if (viewModelCount != null) {
                        // read viewModel.count.getValue()
                        viewModelCountGetValue = viewModelCount.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.count.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelCountGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelCountGetValue);


                    // read Integer.toString(androidx.databinding.ViewDataBinding.safeUnbox(viewModel.count.getValue()))
                    integerToStringViewModelCount = java.lang.Integer.toString(androidxDatabindingViewDataBindingSafeUnboxViewModelCountGetValue);
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.isMessageVisible
                        viewModelIsMessageVisible = viewModel.isMessageVisible();
                    }
                    updateLiveDataRegistration(1, viewModelIsMessageVisible);


                    if (viewModelIsMessageVisible != null) {
                        // read viewModel.isMessageVisible.getValue()
                        viewModelIsMessageVisibleGetValue = viewModelIsMessageVisible.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.isMessageVisible.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelIsMessageVisibleGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelIsMessageVisibleGetValue);
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.numbers
                        viewModelNumbers = viewModel.getNumbers();
                    }
                    updateLiveDataRegistration(2, viewModelNumbers);


                    if (viewModelNumbers != null) {
                        // read viewModel.numbers.getValue()
                        viewModelNumbersGetValue = viewModelNumbers.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 1

            fr.benmeddour.testlocation.BindingAdaptersKt.goneUnless(this.mboundView1, androidxDatabindingViewDataBindingSafeUnboxViewModelIsMessageVisibleGetValue);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, integerToStringViewModelCount);
        }
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.mboundView3.setOnClickListener(mCallback1);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            fr.benmeddour.testlocation.BindingAdaptersKt.numbers(this.mboundView4, viewModelNumbersGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        fr.benmeddour.testlocation.MainViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {


            viewModel.increment();
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.count
        flag 1 (0x2L): viewModel.isMessageVisible
        flag 2 (0x3L): viewModel.numbers
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
    flag mapping end*/
    //end
}
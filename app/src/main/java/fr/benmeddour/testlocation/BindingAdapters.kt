package fr.benmeddour.testlocation

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("numbers")
fun numbers(recyclerView: RecyclerView, items: List<Int>) {
    if (recyclerView.adapter == null) {
        recyclerView.adapter = NumbersAdapter()
    }
    if (recyclerView.layoutManager == null) {
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, RecyclerView.HORIZONTAL, false)
    }
    if (recyclerView.adapter is NumbersAdapter) {
        (recyclerView.adapter as NumbersAdapter).submitList(items)
    }
}
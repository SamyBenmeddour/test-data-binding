package fr.benmeddour.testlocation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    var count = MutableLiveData<Int>()
    var isMessageVisible = MutableLiveData<Boolean>()
    var numbers = MutableLiveData<List<Int>>()

    init {
        count.value = 0
        this.isMessageVisible.value = count.value!! % 2 == 0
        this.numbers.value = emptyList<Int>().toMutableList()
    }

    fun increment() {
        this.count.value = this.count.value!! + 1
        this.isMessageVisible.value = count.value!! % 2 == 0
        val numbers = ArrayList(this.numbers.value!!).also { it.add(this.count.value!!) }
        this.numbers.value = numbers
    }

}